#include "udf.h"
#include "cxndsearch.h"

static ND_Search *domain_table = NULL;

// ��������� ������� ������ ���� ���������������� � cas-�����:
// par_friend - ������� �������: 0 - ������ ��� FRIEND, 1 - ������ � FRIEND
// par_save_data - ������� ���������� dat-����: 0 - �� ���������, 1 - ���������
// par_exit - ������� �������� FLUENT: 0 - �� ���������, 1 - ���������
// ������������� ����������:
// (rp-var-define 'par_friend 0'integer #f)
// (rp-var-define 'par_save_data 0'integer #f)
// (rp-var-define 'par_exit 0'integer #f)

// unixTime (from 1970/01/01) ��������������� CURRENT_TIME=0
// ������ ���� ��������� � dat-�����
int i_astr;

// ����� ������� ����� �� ������� ��������� ���������
int nSaveResult_ed = 1;
int nSaveResult_kd = 1;
int iEndResult = 0;

//test change

// ������� �������������� �������, �������� �������
// 100 - ������������ ���������� ��������
float timeWeather[1000], tempWeather[1000];

int iter_initial = 0;
float cp, lam, ro, rg, intcp, lamSC;
int cp_init = 0;
int lam_init = 0;
int ro_init = 0;
int Rg_init = 0;
int intcp_init = 0;
int lamSC_init = 0;

// for friend ----
real end_time;

// �������� �������������� ������� ������ ������ �� ������ ��������
int status_init = 0;

int status;
int i_timeCalibr;

float timeStepWeather;
int countWeather;

int n_iter_0;

// 2017.11.26 ---------------------------------------------------
// ������� ��������� ���������� ������
//  forFriend - ������� ������ ��� FRIEND
//  =0 - FRIEND �� ����������
//  =1 - FRIEND ����������

//  type_exper - ��� ��������� ���������
//  =1 - �����������
//  =2 - ����������
//  =3 - ������������
//  =4 - �����
//	=5 - ��������
//	=6 - ��������

//  type_geo - ��� ��������� 
//  =1 - �����������
//  =2 - �����
//  =3 - �����


int forFriend[20];
int type_exper[20];
int type_geo[20];
int IDs[20];
float xPoint[20];
float yPoint[20];
float zPoint[20];

int count_exp_par;
// -----------------------------------------------------

// ������ ���������� ���������� ������ FLUENT
// ��� ������ ��������
DEFINE_ADJUST(adjust_prepare, d)
{
	FILE *fp;
	FILE *fp1;
	int i;

	int n;

	// ������ ��� Friend
	// ���������� par_friend=0
	RP_Set_Integer("par_friend", 0);


	// ���� ������ �� ���������
	if (status == -1)
	{
		// ������� ���� status_ready.txt
		// ���������� ������ = 0
		// dat-���� �� ���������
		fp1 = fopen("status_ready.txt", "w");
		fclose(fp1);
		status = 0;
		Message("status ready *********....... \n");
		RP_Set_Integer("par_save_data", 0);
	}

	// ���� ������ ��������
	if (status_init == 0)
	{
		remove("fluent_out.txt");
		// �������� ������ �� �����
		// ��������� ������	   
		fp = fopen("status.txt", "r");
		fscanf(fp, "%d\n", &status);
		Message("  %d\n", status);
		fclose(fp);
		// ������ ��� FRIEND
		// �������� �������� ����������� �������� ������
		if (status == 0)
		{
			fp = fopen("calibration.txt", "r");
			while (feof(fp) == 0)
			{
				fscanf(fp, "%f\n", &timeStepWeather);
				Message(" weather time step = %f\n", timeStepWeather);
			}
			fclose(fp);
		}

		// ������ ��� FRIEND
		// �������� ������(�) �������� ������ �� ����� �������� ������
		if (status == 0)
		{
			fp = fopen("Weather_dat.txt", "r");
			i = 0;
			while (feof(fp) == 0)
			{
				fscanf(fp, "%f  %f\n", &timeWeather[i], &tempWeather[i]);
				Message("%f   %f\n", timeWeather[i], tempWeather[i]);
				i = i + 1;
			}
			fclose(fp);
		}

		// ������ � FRIEND
		// �������� ������(�) �������� ������ �� ����� �������� ������,
		// ��������������� ��� ��� ������ � FRIEND
		if (status == 1)
		{

			fp = fopen("Weather_friend_dat.txt", "r");
			i = 0;
			while (feof(fp) == 0)
			{
				fscanf(fp, "%f  %f\n", &timeWeather[i], &tempWeather[i]);
				Message("%f   %f\n", timeWeather[i], tempWeather[i]);
				end_time = timeWeather[i]; //������ *3600

										   //Dima ������� ��� ������ ������ � ������� ��� ����� �����-������
				Message("***** end_time = %f\n", end_time);
				i = i + 1;
			}
		}

		countWeather = i;
		status_init = 1;

		// �������� FLUENT �� ���������
		RP_Set_Integer("par_exit", 0);

		// ��������� ����� �������� �� �������
		n_iter_0 = N_TIME;

		// 2017.11.26 ---------------------------------------------------
		fp = fopen("exper_locations.txt", "r");
		count_exp_par = 0;
		while (feof(fp) == 0)
		{
			fscanf(fp, "%d  %d  %d", &forFriend[count_exp_par], &type_exper[count_exp_par], &type_geo[count_exp_par]);
			if (type_geo[count_exp_par] == 1)
			{
				fscanf(fp, "%d\n", &IDs[count_exp_par]);
			}

			if (type_geo[count_exp_par] == 2)
			{
				fscanf(fp, "%d\n", &IDs[count_exp_par]);
			}

			if (type_geo[count_exp_par] == 3)
			{
#if RP_2D
				fscanf(fp, "%f %f\n", &xPoint[count_exp_par], &yPoint[count_exp_par]);
#endif

#if RP_3D
				fscanf(fp, "%f %f %f\n", &xPoint[count_exp_par], &yPoint[count_exp_par], &zPoint[count_exp_par]);
#endif
			}

			count_exp_par = count_exp_par + 1;
		}
		fclose(fp);
		// ------------------------------------------------------
	}
}

// ������ �������� �������� ������
DEFINE_EXECUTE_AT_END(wait_weather)
{
	FILE *fp = NULL;
	FILE *fp1;
	FILE *fp2 = NULL;
	FILE *fp3;

	FILE *fp33;

	float timeCurrent;
	real time = RP_Get_Real("flow-time");
	real time_step = RP_Get_Real("physical-time-step");
	int i;

	// �������� ������ �� �����
	// ��������� ������    
	fp3 = fopen("status.txt", "r");
	fscanf(fp3, "%d\n", &status);
	Message("  %d\n", status);
	fclose(fp3);

	// �������� ����������� ������ ���������
	fp2 = fopen("Experiment_ready.txt", "r");

	// ���� ������ ��������� ���������
	// ���������� ������ ���������������� 
	// �������� ������ � ��� 
	// ���������� ���������� dat-�����
	if (fp2)
	{
		fclose(fp2);
		Message("Waiting status ....... \n");
		status = -1;
		remove("Experiment_ready.txt");
		RP_Set_Integer("par_save_data", 1);
	}


	// ������ ��� FRIEND
	// ������ ��������� �� ��������� 
	if (status == 0)
	{
		// �������� ��������� ����� ��������� �������� ������
		fp1 = fopen("Weather_current.txt", "r");
		fscanf(fp1, "%f\n", &timeCurrent);
		fclose(fp1);

		// ��������� ����������� ���������� ����������
		Message("  %f  %f  %f\n", time, time_step, timeCurrent);
		//if ((time + time_step) >= (timeCurrent*3600 + timeStepWeather))
		if ((time + time_step) > (timeCurrent * 3600))
		{
			Message("Waiting weather data ....... \n");
			// ���� ���������� �� ��������� ����������� �������� ������
			while (!fp)
			{
				fp = fopen("Weather_dat_ready.txt", "r");
			}
			fclose(fp);
			remove("Weather_dat_ready.txt");

			// ���������� �������� ������ � ������� �������� ������		     	     	     
			fp = fopen("Weather_dat.txt", "r");
			i = 0;
			while (feof(fp) == 0)
			{
				fscanf(fp, "%f  %f\n", &timeWeather[i], &tempWeather[i]);
				Message("  %f   %f\n", timeWeather[i], tempWeather[i]);

				//��� ������ ������������ ���������� ������������� ������
				fp33 = fopen("Weather_dat_control.txt", "a");
				fprintf(fp33, "%f %f\n", timeWeather[i], tempWeather[i]);
				fclose(fp33);

				i = i + 1;
			}
			fclose(fp);
			countWeather = i;
			i_timeCalibr = i - 1;
		}
	}
}

// ������ ���������� ������ � FRIEND
DEFINE_EXECUTE_AT_END(friend)
{
	FILE *fp2 = NULL;
	FILE *fp4;

	// ��������� ������ �� �� ��� ������� ������ � FRIEND
	// ���� ������� ������ 
	fp2 = fopen("GoFriend.txt", "r");
	if (fp2)
	{
		fclose(fp2);
		Message("Waiting FRIEND ....... \n");
		remove("GoFriend.txt");

		// �������� � ���� ����� ����� �� ������� ��� �������� FRIEND			
		fp4 = fopen("timesteps.txt", "w");
		fprintf(fp4, " %d\n", N_TIME);
		fprintf(fp4, " %d\n", N_TIME - n_iter_0 + 1);
		fclose(fp4);
		n_iter_0 = N_TIME;

		// ���������� �������� FLUENT
		RP_Set_Integer("par_exit", 1);
	}
}

// 2017.11.26 -- ����� ����������� ��� FRIEND � ���
DEFINE_EXECUTE_AT_END(out_data_ed)
{
	int i;
	FILE *fp;

	real A[ND_ND];
	real s_face;
	real total_s;
	real total_v;
	real sum;

	real time = RP_Get_Real("flow-time");
	real time_step = RP_Get_Real("physical-time-step");
	// ���� ������ ������� ����������������� ����� (������ ����. ���������� ������ ������� ���. ������, ���� ����������, ���������� �����)
	real time_exper[4] = { 8756,10944,13864,17512 };
	int count_exper_time = 4;
	int k;

#if !RP_HOST
	cell_t cp;
	Thread *tp;
	CX_Cell_Id *cx_cell;
#endif

	real P[3];

#if !RP_HOST
	Domain *d;
	Thread *t;
	face_t f;
	cell_t c;
	d = Get_Domain(1);
#endif		

	FILE *fp1;
	char filename[40];

	/*remove("fluent_out.txt");*/

	for (k = 0; k < count_exper_time; k++)
	{
		for (i = 0; i < count_exp_par; i++)
		{
			if (type_geo[i] == 1)
			{
				host_to_node_int_1(IDs[i]);
				total_s = 0;
				sum = 0;
#if !RP_HOST
				t = Lookup_Thread(d, IDs[i]);
				begin_f_loop(f, t)
				{
					if (PRINCIPAL_FACE_P(f, t))
					{
						F_AREA(A, f, t);
						s_face = NV_MAG(A);
						total_s += s_face;

						if (type_exper[i] == 1)
						{
							sum += (F_T(f, t) - 273.15)*s_face;
						}

						if (type_exper[i] == 2)
						{
							//���������
						}

						if (type_exper[i] == 3)
						{
							//������������
						}

						if (type_exper[i] == 4)
						{
							//�����
						}

						if (type_exper[i] == 5)
						{
							//��������
						}

						if (type_exper[i] == 6)
						{
							//��������
						}
					}
				}
				end_f_loop(f, t)

#if RP_NODE
					total_s = PRF_GRSUM1(total_s);
				sum = PRF_GRSUM1(sum);
#endif
#endif
				node_to_host_real_2(total_s, sum);

#if RP_HOST
				if (forFriend[i] == 1)
				{
					if (fabs(time / 3600 - time_exper[k]) < time_step / (3600 * 2))
					{
						fp = fopen("fluent_out.txt", "a");
						fprintf(fp, " %f\n", sum / total_s);
						fclose(fp);
					}
				}

				sprintf(filename, "out-%d-%d-id-%d.txt", type_geo[i], type_exper[i], IDs[i]);
				fp1 = fopen(filename, "a");
				fprintf(fp1, " %f  %f\n", time, sum / total_s);
				fclose(fp1);
#endif
			}

			if (type_geo[i] == 2)
			{
				host_to_node_int_1(IDs[i]);
				total_v = 0;
				sum = 0;
#if !RP_HOST
				t = Lookup_Thread(d, IDs[i]);
				begin_c_loop_int(c, t)
				{
					total_v += C_VOLUME(c, t);

					if (type_exper[i] == 1)
					{
						sum += (C_T(c, t) - 273.15)*C_VOLUME(c, t);
					}

					if (type_exper[i] == 2)
					{
						//���������
					}

					if (type_exper[i] == 3)
					{
						//������������
					}

					if (type_exper[i] == 5)
					{
						//��������
					}

					if (type_exper[i] == 6)
					{
						//��������
					}
				}
				end_c_loop_int(c, t)

#if RP_NODE
					total_v = PRF_GRSUM1(total_v);
				sum = PRF_GRSUM1(sum);
#endif
#endif
				node_to_host_real_2(total_v, sum);

#if RP_HOST
				if (forFriend[i] == 1)
				{
					if (fabs(time / 3600 - time_exper[k]) < time_step / (3600 * 2))
					{
						fp = fopen("fluent_out.txt", "a");
						fprintf(fp, " %f\n", sum / total_v);
						fclose(fp);
					}
				}

				sprintf(filename, "out-%d-%d-id-%d.txt", type_geo[i], type_exper[i], IDs[i]);
				fp1 = fopen(filename, "a");
				fprintf(fp1, " %f  %f\n", time, sum / total_v);
				fclose(fp1);
#endif
			}

			if (type_geo[i] == 3)
			{
#if !RP_HOST
				domain_table = CX_Start_ND_Point_Search(domain_table, TRUE, -1);
				P[0] = xPoint[i];
				P[1] = yPoint[i];
#if RP_3D
				P[2] = zPoint[i];
#endif
				cx_cell = CX_Find_Cell_With_Point(domain_table, P, 0.0);
				if (cx_cell)
				{
					cp = RP_CELL(cx_cell);
					tp = RP_THREAD(cx_cell);

					if (type_exper[i] == 1)
					{
						sum = C_T(cp, tp) - 273.15;
					}

					if (type_exper[i] == 2)
					{
						//���������
					}

					if (type_exper[i] == 3)
					{
						//������������
					}

					if (type_exper[i] == 5)
					{
						//��������
					}

					if (type_exper[i] == 6)
					{
						//��������
					}

					if (forFriend[i] == 1)
					{
						if (fabs(time / 3600 - time_exper[k]) < time_step / (3600 * 2))
						{
							fp = fopen("fluent_out.txt", "a");
							fprintf(fp, " %f\n", sum);
							fclose(fp);

							//Start Logging
							FILE *flog = fopen("log.txt", "a");
							fprintf(flog, "Out_data_ed function: point output value = %f\n", sum);
							fprintf(flog, "Out_data_ed function: N_TIME= %d, CURRENT_TIME=%f,CURRENT_TIMESTEP=%f \n", N_TIME, CURRENT_TIME, CURRENT_TIMESTEP);
							fclose(flog);
							//End Logging
						}
					}

					sprintf(filename, "out-%d-%d-point_%d.txt", type_geo[i], type_exper[i], i);
					fp1 = fopen(filename, "a");
					fprintf(fp1, " %f  %f\n", time, sum);
					fclose(fp1);
				}
				domain_table = CX_End_ND_Point_Search(domain_table);
#endif			
			}
		}
	}
}

// ������ ������ ����������� � ���� �� ������ ����
// ������ ���� ������� ��� �������� �����
// 2017.11.26 --- �������
DEFINE_EXECUTE_AT_END(out_data_kd)
{
	int param_input;

	int count_exp = 1; // ����� ����������������� ������������
	int zone_exp[1] = { 46 }; // ������ ID ������������ (���� � ���)
							  //real temp;
	int i;
	FILE *fp;

	real A[ND_ND];
	real s_face;
	real total_s;
	real total_v;
	real sum;

	real time = RP_Get_Real("flow-time");

#if !RP_HOST
	Domain *d;
	Thread *t;
	face_t f;
	cell_t c;
	d = Get_Domain(1);
#endif		

	param_input = 1;

	if (param_input == 1)
	{
		for (i = 0; i < count_exp; i++)
		{
			host_to_node_int_1(zone_exp[i]);
			total_s = 0;
			sum = 0;
#if !RP_HOST
			t = Lookup_Thread(d, zone_exp[i]);
			begin_f_loop(f, t)
			{
				if (PRINCIPAL_FACE_P(f, t))
				{
					F_AREA(A, f, t);
					s_face = NV_MAG(A);
					total_s += s_face;
					sum += (F_T(f, t) - 273.15)*s_face;
				}
			}
			end_f_loop(f, t)

#if RP_NODE
				total_s = PRF_GRSUM1(total_s);
			sum = PRF_GRSUM1(sum);
#endif
#endif
			node_to_host_real_2(total_s, sum);

			//#if !RP_NODE
#if RP_HOST
			fp = fopen("stenka_kd.out", "a");
			fprintf(fp, " %f   %f\n", time, sum / total_s);
			fclose(fp);
#endif
		}
	}

	/*if (param_input == 2)
	{
	for (i = 0; i < count_exp; i++)
	{
	fp = fopen("fluent_out.txt", "a");
	host_to_node_int_1(zone_exp[i]);
	total_v = 0;
	sum = 0;
	#if !RP_HOST
	t = Lookup_Thread(d, zone_exp[i]);
	begin_c_loop_int(c, t)
	{
	total_v += C_VOLUME(c, t);
	sum += (C_T(c, t) - 273.15)*C_VOLUME(c, t);
	}
	end_c_loop_int(c, t)

	#if RP_NODE
	total_v = PRF_GRSUM1(total_v);
	sum = PRF_GRSUM1(sum);
	#endif
	#endif
	node_to_host_real_2(total_v, sum);

	#if !RP_NODE
	fprintf(fp, " %f   %f\n", time, sum / total_v);
	#endif
	fclose(fp);
	}
	}*/
}

// ������ ������ FLUENT � FRIEND
DEFINE_EXECUTE_AT_END(timeout)
{
	FILE *file_end = NULL;
	FILE *file_begin = NULL;
	FILE *file_fluent;
	FILE *fp2 = NULL;

	//real time_1 = RP_Get_Real("flow-time");

	// ������ � FRIEND	
	if (status == 1)
	{
		if (CURRENT_TIME >= end_time * 3600)
		{
			file_fluent = fopen("fluent_end.txt", "w");
			fclose(file_fluent);

			Message("Waiting end calculation....... \n");
			while (!file_end)
			{
				file_end = fopen("end_calculation.txt", "r");
			}
			fclose(file_end);

			Message("Waiting begin calculation....... \n");
			while (!file_begin)
			{
				file_begin = fopen("begin_calculation.txt", "r");
				// 2017.10.22: AP ���������� ������ � ������ FRIEND
				fp2 = fopen("friend_finish.txt", "r");
				if (fp2)
				{
					fclose(fp2);
					file_begin = fopen("begin_calculation.txt", "w");
					fclose(file_begin);
				}
				//----------------------------------------------
			}
			fclose(file_begin);

			iter_initial = 0;
			cp_init = 0;
			lam_init = 0;
			Rg_init = 0;
			intcp_init = 0;
			lamSC_init = 0;

			//Start Logging
			FILE *flog = fopen("log.txt", "a");
			fprintf(flog, "Timeout function: setting the init state of case: lamSC=%f, intcp=%f, rg=%f,  \n", lamSC, intcp, rg);
			fclose(flog);
			//End Logging

			// �������� �������� ��������� ������ FRIEND			
			fp2 = fopen("friend_finish.txt", "r");
			if (fp2)
			{
				// ���� ������� ������� �� ��������� dat-���� � ��������� FLUENT
				fclose(fp2);
				RP_Set_Integer("par_save_data", 1);
				RP_Set_Integer("par_exit", 1);
			}
			else
			{
				// ���� ������� �� ������� �� ��������� dat-���� � �� ��������� dat-����
				RP_Set_Integer("par_friend", 1);
				RP_Set_Integer("par_save_data", 0);
			}
		}
	}
}

// ������ ������� ������������
DEFINE_SPECIFIC_HEAT(user_cp, T, Tref, h, yi)
{
	real a;
	FILE *fp;
	if (cp_init == 0)
	{
		fp = fopen("param_cp.txt", "r");
		fscanf(fp, "%f\n", &cp);
		Message("  %f\n", cp);
		fclose(fp);
		cp_init = 1;
	}

	a = cp;
	*h = a*(T - Tref);
	return a;
}

// ������ ������� ����������������
DEFINE_PROPERTY(lamda, c, t)
{
	//#if !RP_HOST
	real a;
	FILE *fp;


	if (lam_init == 0)
	{
		fp = fopen("param_a.txt", "r");
		fscanf(fp, "%f\n", &lam);
		Message("  %f\n", lam);
		fclose(fp);
		lam_init = 1;
	}
	a = lam;
	return a;
	//#endif
}

// ������ ������� ���������
DEFINE_PROPERTY(rou, c, t)
{
	real a;
	FILE *fp;


	if (ro_init == 0)
	{
		fp = fopen("param_ro.txt", "r");
		fscanf(fp, "%f\n", &ro);
		Message("  %f\n", ro);
		fclose(fp);
		ro_init = 1;
	}
	a = ro;
	return a;
}
// ������ ������� ����������� �������. �������������
DEFINE_PROFILE(user_Rg, t, i)
{
	//#if !RP_HOST
	FILE *fp;
	cell_t c;

	if (Rg_init == 0)
	{
		fp = fopen("param_rg.txt", "r");
		fscanf(fp, "%f\n", &rg);
		Message("  %f\n", rg);
		fclose(fp);
		Rg_init = 1;

		//Start Logging
		FILE *flog = fopen("log.txt", "a");
		fprintf(flog, "User Rg function: setting the init state of RG = %f\n", rg);
		fclose(flog);
		//End Logging
	}
	begin_c_loop(c, t)
	{
		F_PROFILE(c, t, i) = rg;
	}
	end_c_loop(c, t)
		//#endif
}

real internalCp()
{
	real a;
	FILE *fp;
	if (intcp_init == 0)
	{
		fp = fopen("param_intcp.txt", "r");
		fscanf(fp, "%f\n", &intcp);
		Message("  %f\n", intcp);
		fclose(fp);
		intcp_init = 1;

		//Start Logging
		FILE *flog = fopen("log.txt", "a");
		fprintf(flog, "internalCp() function: setting the init state of scalar cp = %f\n", intcp);
		fclose(flog);
		//End Logging
	}
	a = intcp;
	return a;
}


// ������ ���������� ������� ��� ����������� (�������� ������)
// ������ ���� ������� ��� �������� �����
DEFINE_PROFILE(wall_temp, t, p)
{
	int i;
	face_t f;
	real t_cur = RP_Get_Real("flow-time");
	real a;

	a = 293.;

	for (i = 0; i < countWeather; i++)
	{
		// !!! ���� �������� ���� countWeather = 1	
		if (countWeather != 1)
		{
			if ((t_cur >= timeWeather[i] * 3600) && (t_cur < timeWeather[i + 1] * 3600))
			{
				a = tempWeather[i] + 273.15;
			}
		}
		else
		{
			a = tempWeather[i] + 273.15;
		}

		if (t_cur >= timeWeather[countWeather - 1] * 3600)
		{
			a = tempWeather[countWeather - 1] + 273.15;
		}

	}
	begin_f_loop(f, t)
	{
		F_PROFILE(f, t, p) = a;
	}
	end_f_loop(f, t)
}

// ������ ������ � dat-���� ���������������� ������� ���������������� CURRENT_TIME=0
// ��������������� ����� �� ��������� �� ��������� �����
// ��� ������ ���������� dat-����� ��� ����������������
DEFINE_RW_FILE(writer, fp)
{
	FILE *fp1;
	fp1 = fopen("astronTime.txt", "r");
	fscanf(fp1, "%d\n", &i_astr);
	fclose(fp1);

	Message("savin to dat....... \n");
	fprintf(fp, "%d", i_astr);
}

// ������ ������ �� dat-����� ���������������� ������� ���������������� CURRENT_TIME=0
DEFINE_RW_FILE(read, fp)
{
	FILE *fp1;
	Message("reading from dat....... \n");
	fscanf(fp, "%d", &i_astr);
	Message("%d   %f\n", i_astr, CURRENT_TIME / 3600);

	fp1 = fopen("astronTime.txt", "w");
	fprintf(fp1, "%d\n", i_astr);
	fclose(fp1);
}

// 
DEFINE_RW_FILE(read_time, fp)
{
	Message("reading from dat....... \n");

}

DEFINE_INIT(initialize_rereading, d)
{
	cp_init = 0;
	lam_init = 0;
	ro_init = 0;
	Rg_init = 0;
	intcp_init = 0;
	lamSC_init = 0;
}











#include "udf.h"
#include "surf.h"
#include "sg_mphase.h"
#include "sg.h"

int ID_wall_count = 0;
int id_wall[8] = { 9,10,11,12,7,8,15,16 };

int ID_volume_count;
int id_volume[30];
float x1_w[30], x2_w[30], y1_w[30], y2_w[30];
float y_w[30], h_w[30];
float init_water[30];
float s_zerkalo[30];
real vol_w[30];


int id_init = 0;
real mass_m2 = 0;

real k_ispar = 4.17e-8;
real k_kond = 4.17e-7;
real relaxation;		//   relaxation (dempher) for evaporation/condensation

real t_prev = 0;
real t_cur;
real t_delta;

//
//
//
//---For 2temperature model (2tm 29.08.17)
//
//
//
//---For 2temperature model (2tm 29.08.17)
int mem1 = 2;		// memory indexes for sources

					// ������������ ������ ������� �� �����
real rt = 2400;		// ��������� ������
real relax_2TM = 0.05;	// ���������� ��� �������

real alfa = 1.5;
real A_fs = 0.4276;		// ����������� ��� ���� (������� ����/����� �����)

DEFINE_SOURCE(q_source1, c, t, dS, eqn)
{
	return  C_R(c, t)*A_fs * C_UDMI(c, t, mem1);
}

DEFINE_SOURCE(q_source2, c, t, dS, eqn)		// �������� ������� ��� �������
{
	C_UDMI(c, t, mem1) = C_UDMI(c, t, mem1) + relax_2TM *(alfa *(C_T(c, t) - 273.15 - C_UDSI(c, t, 0) / (internalCp()*rt)) - C_UDMI(c, t, mem1));
	return -A_fs * C_UDMI(c, t, mem1);
}

DEFINE_DIFFUSIVITY(dif, c, t, i)
{
	real a;
	FILE *fp;
	if (lamSC_init == 0)
	{
		fp = fopen("param_lamSC.txt", "r");
		fscanf(fp, "%f\n", &lamSC);
		Message("  %f\n", lamSC);
		fclose(fp);
		lamSC_init = 1;

		//Start Logging
		FILE *flog = fopen("log.txt", "a");
		fprintf(flog, "User DIF function: setting the init state of scalar lambda  = %f\n", lamSC);
		fclose(flog);
		//End Logging

	}
	a = lamSC / (internalCp()*rt);
	return a;
}


//----------------------------------------
// ������ �� ������� � �������
// ������������ � ��������� �������� ��� �������: �������� ������� (�� �����!!!)
DEFINE_PROFILE(boundary_t_scalar, t, nv)
{
	face_t f;

	begin_f_loop(f, t)
	{
		F_PROFILE(f, t, nv) = (F_T(f, t) - 273.15)*internalCp()*rt;
	}
	end_f_loop(f, t)
}

//-----------------
//-----------------
DEFINE_PROFILE(boundary_t_air, t, nv)
{
	face_t f;			// face 

	Thread *t1;			// pointer to zone thread
	face_t f1;			// cell face
	Thread *t2;			// pointer to zone thread
	cell_t c2;			// cell variable
	Thread *t0;			// pointer to zone thread
	cell_t c0;			// cell variable

	real A[ND_ND], ds, es[ND_ND], A_by_es, dr0[ND_ND];			// ND_ND - number of dimensions

	real lam_air;			// air conductivity
	real lam_sca;			// scalar conductivity in general dimension ( W/(m*K) )
	real lam_sol;			// solid zone conductivity

	real b_air;				// distance from surface to air cell centroid
	real b_sca;				// distance from surface to cell centroid where scalar in fluid zone
	real b_sol;				// distance from surface to solid cell centroid

	real t_air;
	real t_sca;
	real t_sol;

	begin_f_loop(f, t)		// loop through surface boundary zone cell faces
	{
		F_AREA(A, f, t);	// 
		c0 = F_C0(f, t);	//
		t0 = THREAD_T0(t);	//
		BOUNDARY_FACE_GEOMETRY(f, t, A, b_air, es, A_by_es, dr0);		// find the b_air - distance to cell centroid
		b_sca = b_air;													// b_air is the same as b_sca
		lam_air = C_K_L(c0, t0);//C_K_EFF(c0, t0, 0.85);										// find air conductivity (laminar)
		lam_sca = C_UDSI_DIFF(c0, t0, 0)*rt * internalCp();	//!!!
		t_air = C_T(c0, t0);
		t_sca = C_UDSI(c0, t0, 0) / (internalCp()*rt) + 273.15;

		t1 = THREAD_SHADOW(t);			// find opposite surface zone pointer
		t2 = THREAD_T0(t1);				// t2 - opposite volume zone
		f1 = F_SHADOW(f, t);			// possible bug!!!!
		c2 = F_C0(f1, t1);				// possible bug!!!!
		F_AREA(A, f1, t1);
		BOUNDARY_FACE_GEOMETRY(f1, t1, A, b_sol, es, A_by_es, dr0);
		lam_sol = C_K_L(c2, t2);
		t_sol = C_T(c2, t2);

		F_PROFILE(f, t, nv) = (lam_sol*t_sol / b_sol + lam_air*t_air / b_air + lam_sca*t_sca / b_sca) /
			(lam_sol / b_sol + lam_air / b_air + lam_sca / b_sca);;

	}
	end_f_loop(f, t)
}

DEFINE_PROFILE(boundary_t_solid, t, nv)
{
	face_t f;

	Thread *t1;
	face_t f1;
	Thread *t2;
	cell_t c2;
	Thread *t0;
	cell_t c0;

	real A[ND_ND], ds, es[ND_ND], A_by_es, dr0[ND_ND];

	real lam_air;
	real lam_sca;
	real lam_sol;

	real b_air;
	real b_sca;
	real b_sol;

	real t_air;
	real t_sca;
	real t_sol;

	begin_f_loop(f, t)
	{
		c0 = F_C0(f, t);
		t0 = THREAD_T0(t);
		F_AREA(A, f, t);
		BOUNDARY_FACE_GEOMETRY(f, t, A, b_sol, es, A_by_es, dr0);
		lam_sol = C_K_L(c0, t0);
		t_sol = C_T(c0, t0);

		t1 = THREAD_SHADOW(t);
		t2 = THREAD_T0(t1);
		f1 = F_SHADOW(f, t);
		c2 = F_C0(f1, t1);
		F_AREA(A, f1, t1);
		BOUNDARY_FACE_GEOMETRY(f1, t1, A, b_air, es, A_by_es, dr0);
		b_sca = b_air;
		lam_air = C_K_L(c2, t2);
		lam_sca = C_UDSI_DIFF(c2, t2, 0)*rt * internalCp();
		t_air = C_T(c2, t2);
		t_sca = C_UDSI(c2, t2, 0) / (internalCp()*rt) + 273.15;

		F_PROFILE(f, t, nv) = (lam_sol*t_sol / b_sol + lam_air*t_air / b_air + lam_sca*t_sca / b_sca) /
			(lam_sol / b_sol + lam_air / b_air + lam_sca / b_sca);
	}
	end_f_loop(f, t)
}


//
//
//
//---End For For 2temperature model (2tm 29.08.17)
//
//
//


DEFINE_ADJUST(adjust_cond, d)
{
	Thread *t;
	face_t f;
	Thread *t0;
	cell_t c0;

	//	----- ��� ����������� ������ -------------------------------------
	Thread **ph0;
	// -------------------------------------------------------------------
	//	----- ��� �������� -------------------------------------

	int j;

	real A[ND_ND];
	real s_face;

	real abshumidity;
	real tmp;
	real relative_humid;
	real ps;
	real flux;
	real k_rel = 200;
	real relation = 1;

	real koef;
	real koef_v;

	t_cur = RP_Get_Real("flow-time");
	t_prev = PREVIOUS_TIME;

	//	if (t_prev != t_cur)
	{
		t_delta = t_cur - t_prev;


		for (j = 0; j < ID_wall_count; j++)
		{
			t = Lookup_Thread(d, id_wall[j]);

			begin_f_loop(f, t)
			{
				F_AREA(A, f, t);
				s_face = NV_MAG(A);

				c0 = F_C0(f, t);
				t0 = THREAD_T0(t);
				//tmp = C_T(c0,t0) - 273.15;
				tmp = F_T(f, t) - 273.15;
				abshumidity = C_YI(c0, t0, 0)*C_R(c0, t0);
				relative_humid = 1000 * abshumidity / (5.379586E-06*pow(tmp, 4) + 7.072184E-05*pow(tmp, 3) + 9.676106E-03*pow(tmp, 2) + 2.816077E-01*tmp + 3.743141); //������ ���������� ��������� ��� 100%

				ps = (exp(77.345 + 0.0057*(tmp + 273.15) - 7235 / (tmp + 273.15))) / pow((tmp + 273.15), 8.2);

				if (relative_humid <= 1)
				{
					koef = k_ispar;
				}
				else
				{
					koef = k_kond;
				}

				C_UDMI(c0, t0, 0) = -koef * (relative_humid - 1)*ps*s_face;
				//		C_UDMI(c0, t0, 0) = -koef * (relative_humid - 1)*ps*s_face / C_VOLUME(c0, t0);
				//		C_UDMI(c0, t0, 1) = C_UDMI(c0, t0, 0)*C_VOLUME(c0, t0)/ s_face;	
			}
			end_f_loop(f, t)
		}

		for (j = 0; j < ID_wall_count; j++)
		{
			t = Lookup_Thread(d, id_wall[j]);
			begin_f_loop(f, t)
			{
				//------------------------------------------------------------
				c0 = F_C0(f, t);
				t0 = THREAD_T0(t);

				if (C_UDMI(c0, t0, 0) <= 0)
				{
					C_UDMI(c0, t0, 1) = C_UDMI(c0, t0, 1) - C_UDMI(c0, t0, 0) * t_delta;
				}

				if ((C_UDMI(c0, t0, 0) > 0) && (C_UDMI(c0, t0, 1) > 0))
				{
					if (C_UDMI(c0, t0, 0)*t_delta < C_UDMI(c0, t0, 1))
					{
						C_UDMI(c0, t0, 1) = C_UDMI(c0, t0, 1) - C_UDMI(c0, t0, 0)*t_delta;
					}
					else
					{
						C_UDMI(c0, t0, 0) = C_UDMI(c0, t0, 1) / t_delta;
						C_UDMI(c0, t0, 1) = 0;
					}
				}

				if ((C_UDMI(c0, t0, 0) > 0) && (C_UDMI(c0, t0, 1) <= 0))
				{
					C_UDMI(c0, t0, 0) = 0;
					C_UDMI(c0, t0, 1) = 0;
				}
				//---------------------------------------------------------------
			}
			end_f_loop(f, t)
		}

		for (j = 0; j < ID_volume_count; j++)
		{
			t0 = Lookup_Thread(d, id_volume[j]);
			begin_c_loop(c0, t0)
			{
				C_CENTROID(A, c0, t0);

				if ((A[1] >= y_w[j]) && (A[1] <= y_w[j] + h_w[j]))
				{
					if (((A[0] >= x1_w[j]) && (A[0] <= x2_w[j])) && ((A[2] >= y1_w[j]) && (A[2] <= y2_w[j])))
					{

						tmp = (C_UDSI(c0, t0, 0) / (internalCp()*rt) < (C_T(c0, t0) - 273.15)) ? C_UDSI(c0, t0, 0) / (internalCp()*rt) : (C_T(c0, t0) - 273.15);

						//tmp = C_T(c0, t0) - 273.15;		// ����������� ������� � ������	(2tm 29.08.17)
						// ����������� ������� � ������� (����������� ������) (2tm 29.08.17)
						//	----- ��� ����������� ������ -------------------------------------
						//					ph0 = THREAD_SUB_THREADS(t0);
						//					abshumidity = C_YI(c0, ph0[0], 0) * C_VOF(c0, ph0[0]);
						// -------------------------------------------------------------------
						//	----- ��� ���������� ������ --------------------------------------
						abshumidity = C_YI(c0, t0, 0);
						// -------------------------------------------------------------------
						relative_humid = 1000 * abshumidity / (5.379586E-06*pow(tmp, 4) + 7.072184E-05*pow(tmp, 3) + 9.676106E-03*pow(tmp, 2) + 2.816077E-01*tmp + 3.743141); //������ ���������� ��������� ��� 100%
						ps = (exp(77.345 + 0.0057*(tmp + 273.15) - 7235 / (tmp + 273.15))) / pow((tmp + 273.15), 8.2);
						koef = (relative_humid <= 1) ? k_ispar : k_kond;

						flux = vol_w[j] > 0 ? -koef * (relative_humid - 1)*ps*s_zerkalo[j] / vol_w[j] : 0;	// Dimension: kg/(m3*s)

						relaxation = 0.1;
						C_UDMI(c0, t0, 0) = C_UDMI(c0, t0, 0) + relaxation*(flux - C_UDMI(c0, t0, 0));

						if (C_UDMI(c0, t0, 0)*t_delta*C_VOLUME(c0, t0) >= C_UDMI(c0, t0, 1))				// not enough virtual water
						{
							C_UDMI(c0, t0, 0) = C_UDMI(c0, t0, 1) / (C_VOLUME(c0, t0)*t_delta);
						}
					}
				}
			}
			end_c_loop(c0, t0)
		}
	}

}

DEFINE_SOURCE(mass_cond, c, t, dS, eqn)
{
	return  C_UDMI(c, t, 0);
}

DEFINE_SOURCE(heat_cond, c, t, dS, eqn)
{
	int j;
	real L = 0; //2.5e6;
	real m_source;
	real x[ND_ND];
	m_source = 0;

	for (j = 0; j < ID_volume_count; j++) {
		C_CENTROID(x, c, t);
		if ((x[1] >= y_w[j]) && (x[1] <= y_w[j] + h_w[j])) {
			if (((x[0] >= x1_w[j]) && (x[0] <= x2_w[j])) && ((x[2] >= y1_w[j]) && (x[2] <= y2_w[j]))) {
				m_source = (C_UDMI(c, t, 0) / vol_w[j]) * (-L + C_CP(c, t) * (C_T(c, t) - 298.15));
			}
		}
	}
	return 0;// m_source;
}

DEFINE_ON_DEMAND(read_water_initial)
{
#if !RP_HOST
	Domain *d;
	cell_t c;
	Thread *t;
#endif

	FILE *fp;

	real x[ND_ND];

	int n;
	int i = 0;

	int id;
	float x1, x2, y1, y2;
	float y, h;
	float init_source;
	float s_zerk;

	real vol;

#if !RP_HOST
	d = Get_Domain(1);
#endif

	fp = fopen("water_initial.txt", "r");

	//    	n_por = 0;

	while (feof(fp) == 0)
	{
		fscanf(fp, "%d %f %f %f %f %f %f %f  %f\n", &id, &y, &h, &x1, &x2, &y1, &y2, &init_source, &s_zerk);
		Message("%d %f %g   %g  %g  %g %g  %g  %g\n", id, y, h, x1, x2, y1, y2, init_source, s_zerk);

		ID_volume_count = i + 1;
		id_volume[i] = id;
		y_w[i] = y;
		h_w[i] = h;
		x1_w[i] = x1;
		x2_w[i] = x2;
		y1_w[i] = y1;
		y2_w[i] = y2;
		init_water[i] = init_source;


		n = 0;
		vol = 0;
		host_to_node_int_1(id);

#if !RP_HOST		
		t = Lookup_Thread(d, id);

		begin_c_loop_int(c, t)
		{
			C_CENTROID(x, c, t);

			if ((x[1] >= y) && (x[1] <= y + h))
			{
				if (((x[0] >= x1) && (x[0] <= x2)) && ((x[2] >= y1) && (x[2] <= y2)))
				{
					C_UDMI(c, t, 1) = init_source;
					vol = vol + C_VOLUME(c, t);
				}
			}
		}
		end_c_loop_int(c, t)

#if RP_NODE
			vol = PRF_GRSUM1(vol);
#endif
#endif
		node_to_host_real_1(vol);

#if !RP_HOST		
		t = Lookup_Thread(d, id);
		begin_c_loop(c, t)
		{
			C_CENTROID(x, c, t);

			if ((x[1] >= y) && (x[1] <= y + h))
			{
				if (((x[0] >= x1) && (x[0] <= x2)) && ((x[2] >= y1) && (x[2] <= y2)))
				{
					n = n + 1;
					C_UDMI(c, t, 1) = C_UDMI(c, t, 1) / vol * C_VOLUME(c, t);
					Message("num=  %d  vol_cell= %g  vol_total = %g  mass = %g\n", n, C_VOLUME(c, t), vol, C_UDMI(c, t, 1));
				}
			}
		}
		end_c_loop(c, t)
#endif	

			vol_w[i] = vol;
		s_zerkalo[i] = s_zerk;
		//	ID_volume_count = i;
		i = i + 1;
	}
	fclose(fp);
}

DEFINE_EXECUTE_AT_END(sum_masses)
{
	Domain *d;
	cell_t c0;
	Thread *t0;
	int j;
	real koef_v;
	real A[ND_ND];
	d = Get_Domain(1);


	for (j = 0; j < ID_volume_count; j++)
	{
		t0 = Lookup_Thread(d, id_volume[j]);
		begin_c_loop(c0, t0)
		{
			C_CENTROID(A, c0, t0);

			if ((A[1] >= y_w[j]) && (A[1] <= y_w[j] + h_w[j]))
			{
				if (((A[0] >= x1_w[j]) && (A[0] <= x2_w[j])) && ((A[2] >= y1_w[j]) && (A[2] <= y2_w[j])))
				{
					C_UDMI(c0, t0, 1) -= C_UDMI(c0, t0, 0) * C_VOLUME(c0, t0) * t_delta;
				}
			}
		}
		end_c_loop(c0, t0)
	}
}


// ������ ������� � ������ ��� ������� ������������ ������:
// ������������������ ����������� (0�), �������������� ��������� (%), ����������� ����� (��), 
// ������ ������������ ������ (�3), ������������ ������ ���� (��/�3)
DEFINE_EXECUTE_AT_END(out_udm_1)
{
	FILE *fout;
	int j;
	real x[ND_ND];
	int id;

	real sum_udm;
	real aver_temp;
	real aver_relat_hum;
	real aver_vof;

	float x1, x2, y1, y2, y, h;
	real tmp;
	real abshumidity;
	real relative_humid;
	real ps;

	//	----- ��� ����������� ������ -------------------------------------
	Thread **ph0;
	// -------------------------------------------------------------------

	real time = RP_Get_Real("flow-time");
	fout = fopen("mass_udm_1.txt", "a");

#if !RP_HOST	
	Domain *d;
	Thread *t;
	cell_t c;
#endif

#if !RP_HOST
	d = Get_Domain(1);
#endif
	//	Message("-------- ID_volume_count = %d\n", ID_volume_count);
	for (j = 0; j < ID_volume_count; j++)
	{
		id = id_volume[j];
		//		Message("-------- j=  %d  id = %d\n", j, id);
		host_to_node_int_1(id);
		sum_udm = 0;
		aver_temp = 0;
		aver_relat_hum = 0;
		aver_vof = 0;
#if !RP_HOST
		t = Lookup_Thread(d, id);

		begin_c_loop_int(c, t)
		{
			C_CENTROID(x, c, t);

			x1 = x1_w[j];
			x2 = x2_w[j];
			y1 = y1_w[j];
			y2 = y2_w[j];
			y = y_w[j];
			h = h_w[j];

			if ((x[1] >= y) && (x[1] <= y + h))
			{
				if (((x[0] >= x1) && (x[0] <= x2)) && ((x[2] >= y1) && (x[2] <= y2)))
				{
					sum_udm = sum_udm + C_UDMI(c, t, 1);
					tmp = C_T(c, t) - 273.15;
					aver_temp = aver_temp + tmp*C_VOLUME(c, t);
					//	----- ��� ����������� ������ -------------------------------------
					ph0 = THREAD_SUB_THREADS(t);
					abshumidity = C_YI(c, ph0[0], 0)* C_VOF(c, ph0[0]); // change 23.06.2017
																		// -------------------------------------------------------------------
					relative_humid = 1000 * abshumidity / (5.379586E-06*pow(tmp, 4) + 7.072184E-05*pow(tmp, 3) + 9.676106E-03*pow(tmp, 2) + 2.816077E-01*tmp + 3.743141); //������ ���������� ��������� ��� 100%
					ps = (exp(77.345 + 0.0057*(tmp + 273.15) - 7235 / (tmp + 273.15))) / pow((tmp + 273.15), 8.2);
					aver_relat_hum = aver_relat_hum + ps*C_VOLUME(c, t);
					aver_vof = aver_vof + C_VOF(c, ph0[1])*C_R(c, t);
				}
			}
		}
		end_c_loop_int(c, t)
#if RP_NODE
			sum_udm = PRF_GRSUM1(sum_udm);
		aver_temp = PRF_GRSUM1(aver_temp);
		aver_relat_hum = PRF_GRSUM1(aver_relat_hum);
		aver_vof = PRF_GRSUM1(aver_vof);
#endif
#endif
		node_to_host_real_1(sum_udm);
		node_to_host_real_1(aver_temp);
		node_to_host_real_1(aver_relat_hum);
		node_to_host_real_1(aver_vof);
		node_to_host_int_1(id);

#if !RP_NODE
		Message("num id=  %d  temp = %f  relat_hum = %f  mass = %f  vol = %f  vof2 = %f\n", id, aver_temp / vol_w[j], aver_relat_hum / vol_w[j], sum_udm, vol_w[j], aver_vof / vol_w[j]);
		fprintf(fout, "%d  %f  %f  %f  %f  %f\n", id, aver_temp / vol_w[j], aver_relat_hum / vol_w[j], sum_udm, vol_w[j], aver_vof / vol_w[j]);
#endif
	}
	fclose(fout);
}


// ������� ������� �������� �����
DEFINE_ON_DEMAND(read_porous_property)
{
	Domain *d;
	cell_t c;
	Thread *t;
	FILE *fp;

	real x[ND_ND];

	int n = 1;
	int i;

	int id;
	float x1, x2, y1, y2;
	float y, h;
	float defRou, defLam, defMu, defPor;
	float Rou, Lam, Mu, Por;

	d = Get_Domain(1);

	fp = fopen("porous_property.txt", "r");

	fscanf(fp, "%f %f %f %f \n", &defRou, &defLam, &defMu, &defPor);
	Message("%g   %g  %g  %g\n", defRou, defLam, defMu, defPor);

	thread_loop_c(t, d)
	{
		begin_c_loop(c, t)
		{
			C_UDMI(c, t, 2) = defRou;
			C_UDMI(c, t, 3) = defLam;
			C_UDMI(c, t, 4) = defMu;
			C_UDMI(c, t, 5) = defPor;
		}
		end_c_loop(c, t)
	}

	while (feof(fp) == 0)
	{
		fscanf(fp, "%d %f %f %f %f %f %f %f %f %f  %f\n", &id, &y, &h, &x1, &x2, &y1, &y2, &Rou, &Lam, &Mu, &Por);
		Message("%d %f %g   %g %g  %g  %g %g %g  %g  %g\n", id, y, h, x1, x2, y1, y2, Rou, Lam, Mu, Por);

		t = Lookup_Thread(d, id);

		begin_c_loop(c, t)
		{
			C_CENTROID(x, c, t);

			if ((x[1] >= y) && (x[1] <= y + h))
			{
				if (((x[0] >= x1) && (x[0] <= x2)) && ((x[2] >= y1) && (x[2] <= y2)))
				{
					C_UDMI(c, t, 2) = Rou;
					C_UDMI(c, t, 3) = Lam;
					C_UDMI(c, t, 4) = Mu;
					C_UDMI(c, t, 5) = Por;
				}
			}
		}
		end_c_loop(c, t)

	}

	fclose(fp);
}
// ��������� ��������� �������� �����
DEFINE_PROPERTY(porous_rou, c, t)
{
	real a;
	a = C_UDMI(c, t, 2);
	return a;
}

// ���������������� ��������� �������� �����
DEFINE_PROPERTY(porous_lam, c, t)
{
	real a;
	a = C_UDMI(c, t, 3);
	return a;
}

// ���������� ������������� �������� �����
DEFINE_PROFILE(porous_mu, t, i)
{
	cell_t c;

	begin_c_loop(c, t)
	{
		F_PROFILE(c, t, i) = C_UDMI(c, t, 4);
	}
	end_c_loop(c, t)
}

// ���������� �������� �����
DEFINE_PROFILE(porous_porosity, t, i)
{
	cell_t c;

	begin_c_loop(c, t)
	{
		F_PROFILE(c, t, i) = C_UDMI(c, t, 5);
	}
	end_c_loop(c, t)
}


DEFINE_PROFILE(wind_velocity, t, nv)
{
	face_t f;
	real x[ND_ND];
	real u;

	// ��� ������� ������� �������� �� ������
	real z1 = 10;
	real z0 = 1.2;
	real u1 = 4;

	int n = 2; /* ����� ����� */

	real value[2] = { 0.80, 0.80 };

	real time[2] = { 0, 1000000000 };

	int i;
	real a, b;

	real flow_time = RP_Get_Real("flow-time");

	for (i = 0; i<n - 1; i++)
	{
		if ((flow_time >= time[i]) && (flow_time<time[i + 1]))
		{
			a = (value[i + 1] - value[i]) / (time[i + 1] - time[i]);
			b = value[i] - a*time[i];
		}
	}

	u1 = a*flow_time + b;

	/*
	begin_f_loop(f, t)
	{
	F_CENTROID(x, f, t);
	u = (u1*(log(x[1] / z0))) / (log(z1 / z0));
	F_PROFILE(f, t, nv) = u;
	}
	end_f_loop(f, t)*/

	begin_f_loop(f, t)
	{
		F_CENTROID(x, f, t);
		if ((x[1] >= 15.6) && (x[1] <= 31.8)) //25.633334
		{
			u = 0;
		}
		if (x[1] > 31.8) //25.633334
		{
			//u = (u1*(log(x[1] / z0))) / (log(z1 / z0)); - ������������ �������
			//u = (u1*(log((((x[1] - 24.433334) + fabs(x[1] - 24.433334)) / 2) / z0))) / (log(z1 / z0));
			u = (u1*(log((((x[1] - 30.6) + fabs(x[1] - 30.6)) / 2) / z0))) / (log(z1 / z0));

			F_PROFILE(f, t, nv) = u;
		}
	}
	end_f_loop(f, t)
}
// ��� ������� ������� �������� �� ������
//

// �������� ������ ��� ����� ������������� � ������ Mixture
DEFINE_VECTOR_EXCHANGE_PROPERTY(custom_slip, c, mixture_thread, second_column_phase_index, first_column_phase_index, vector_result)
{
	real grav[2] = { 0., -9.81 };
	real K = 5.e4;
	//	real pgrad_x, pgrad_y;

	real d = 5e-6;
	real dens = 2000;
	real mu = 1.8e-5;

	Thread *pt, *st;/* thread pointers for primary and secondary phases*/

	pt = THREAD_SUB_THREAD(mixture_thread, second_column_phase_index);
	st = THREAD_SUB_THREAD(mixture_thread, first_column_phase_index);

	/* at this point the phase threads are known for primary (0) and
	secondary(1) phases */

	// pgrad_x = 1;
	// pgrad_y = 1; 

	// vector_result[0] = -(pgrad_x/K)+( ((C_R(c, st)-C_R(c, pt))/K)*grav[0]);  
	// vector_result[1] =  -(pgrad_y/K)+( ((C_R(c, st)-C_R(c, pt))/K)*grav[1]); 

	vector_result[0] = 0;
	vector_result[1] = -2 * grav[1] * (d * d / 4) *(C_R(c, pt) - C_R(c, st)) / (9 * mu);
	// Message("second=%f first=%e\n", C_R(c, pt), C_R(c, st));
}